const csv = require('csv-parser');
const fs = require('fs');
const ipl = require('../server/ipl');

let matchesData = [];
let deliveriesData = [];

// parse the matches.csv

 fs.createReadStream('src/data/matches.csv')
  .pipe(csv())
  .on('data', (data) => matchesData.push(data))
  .on('end', () => {
    // call matches per year function

     let yearlyMatchesData = JSON.stringify(ipl.matchesPerYear(matchesData));

     fs.writeFile('src/public/output/matchesPerYear.json',yearlyMatchesData,(err)=>{
         if(err) throw err;
     })
    
     
     console.log("matchesPerYear data written into json file");
     
     // call wonMatchesPerTeamByPerYear 
     const result = ipl.wonMatchesPerTeamByPerYear(matchesData);
     const wonMatchesByTeam = JSON.stringify(result);
     
     fs.writeFile('src/public/output/wonMatchesPerTeamByPerYear.json',wonMatchesByTeam,(err)=>{
        if(err) throw err;
    })
     console.log("wonMatchesPerTeamByPerYear data written into json file");

     // // Find the number of times each team won the toss and also won the match calling this function
     const result6 = ipl.eachTeamWonTossWithMatch(matchesData);
     fs.writeFile('src/public/output/eachTeamWonTossWithMatch.json',JSON.stringify(result6),(err)=> {
       if(err) throw err;
     })

     //call playerWonHighestPlayerOfMatch function
     const result7 = ipl.playerWonHighestPlayerOfMatch(matchesData);
     fs.writeFile('src/public/output/playerWonHighestPlayerOfMatch.json',JSON.stringify(result7),(err)=> {
       if(err) throw err;
     })

// parse the deliveries.csv

fs.createReadStream('src/data/deliveries.csv')
  .pipe(csv())
  .on('data', (data) => deliveriesData.push(data))
  .on('end', () => {
      let result = ipl.extraRunConcededbyPerTeam2016(matchesData, deliveriesData);

      fs.writeFile('src/public/output/extraRunConcededbyPerTeam2016.json',JSON.stringify(result),(err) => {
        if(err) throw err;
      })
      
      console.log("extraRunConcededbyPerTeam2016 data written into json file");

      let result2 = ipl.topTenEcoBowler2015(matchesData,deliveriesData);
      fs.writeFile('src/public/output/topTenEcoBowler2015.json',JSON.stringify(result2),(err)=> {
        if(err) throw err;
      })

      console.log('topTenEcoBowler2015 data written into json file');

      // calling of strikeRateOfBatsmanForEachSeason function

      const highestDiss = ipl.highNoOfTimePlayerDisByAnotherPlayer(deliveriesData);
      fs.writeFile('src/public/output/highNoOfTimePlayerDisByAnotherPlayer.json',JSON.stringify(highestDiss),(err)=> {
        if(err) throw err;
      })

// calling best econ super over boller;
     const bestBowlerInSuperOver = ipl.bestEcoBowlerInSuperOver(deliveriesData);
     fs.writeFile('src/public/output/bestEcoBowlerInSuperOver.json',JSON.stringify(bestBowlerInSuperOver),(err)=>{
       if(err) throw err;
     })
// calling strikeRateOfBatsmanForEachSeason function
     const strikeRateOfBatsman = ipl.strikeRateOfBatsmanForEachSeason(matchesData,deliveriesData);
     fs.writeFile('src/public/output/strikeRateOfBatsmanForEachSeason.json',JSON.stringify(strikeRateOfBatsman),(err)=>{
      if(err) throw err;
    })
  });

  });