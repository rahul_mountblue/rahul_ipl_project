// Number of matches played per year for all the years in IPL.
function matchesPerYear(matches) {
  let obj = {};
  matches.map((data)=>{

          if(obj[data.season]) {
            obj[data.season]++;
          } else {
            obj[data.season] = 1;
          }
           
  })
  return obj;
}

// function for every year won matches by every team

function wonMatchesPerTeamByPerYear(matches) {
  let obj = {};

  matches.map((data)=> {
   
      // check if winner team is added in our object or not if not run else part 
      if(obj[data.winner]) {

        // check winner team season is added into object or not if not add into abject

          if(obj[data.winner][data.season]){
            obj[data.winner][data.season]++
          } else {
            obj[data.winner][data.season] = 1;
          }
      } else {

// add winner team into object and season initial with 1

          if(data.winner !== '')
          {
            obj[data.winner] = {}
            obj[data.winner][data.season] = 1;
          }
        }
  })
  return obj;
}

// Extra run conceded by every team in 2016

function extraRunConcededbyPerTeam2016(matchesData, deliveriesData) {
 // filter out all matches happened in year 2016
  let filterMatchesData = matchesData.filter((data)=>  data.season === '2016')
  let len = filterMatchesData.length;
  let startValue = filterMatchesData[0].id

  // filter out all the deliveries that boll in the year 2016
  let filterDeliveriesData = deliveriesData.filter((data)=> parseInt(data.match_id) >= startValue && parseInt(data.match_id) <= filterMatchesData[len-1].id); 
    let obj = {};

    // populate empty object with bowling team name and extra runs conceded by it.
     
    filterDeliveriesData.map((data)=>{
      if(obj[data.bowling_team]) {
        
        if(obj[data.bowling_team].extra_run_2016) {
          obj[data.bowling_team].extra_run_2016 += parseInt(data.extra_runs); 
        } else {
          obj[data.bowling_team].extra_run_2016 = parseInt(data.extra_runs);  
        }

      } else {
             obj[data.bowling_team] = {};
             obj[data.bowling_team].extra_run_2016 = parseInt(data.extra_runs);  
      }

    })

return obj;

}

// function for top ten economical bowler in season 2015

function topTenEcoBowler2015(matchesData, deliveriesData) {
      
  let filterMatchesData = matchesData.filter((data)=>  data.season === '2015')
  let len = filterMatchesData.length;
  let startValue = filterMatchesData[0].id

  let filterDeliveriesData = deliveriesData.filter((data)=> parseInt(data.match_id) >= startValue && parseInt(data.match_id) <= filterMatchesData[len-1].id);

  // initialize empty object
  let obj = {};
  // populate empty object with bowlername, totalRuns and totalBalls
 filterDeliveriesData.map((data)=> {
   let totalRuns = parseInt(data.total_runs);
   if(obj[data.bowler]) {
      
    if(data.wide_runs === '0' && data.noball_runs === '0') {

      obj[data.bowler].totalBalls++;
      obj[data.bowler].totalRuns += totalRuns

     } else {

      obj[data.bowler].totalRuns += totalRuns

     }

   } else {
// first add bowler into object

     obj[data.bowler] = {};
     if(data.wide_runs === '0' && data.noball_runs === '0') {

      obj[data.bowler].totalBalls = 1;
      obj[data.bowler].totalRuns = totalRuns

     } else {
      obj[data.bowler].totalBalls = 0;   
      obj[data.bowler].totalRuns = totalRuns

     }
    }
 })

 // calculate economy of every bowler

 Object.keys(obj).map((key)=> {
   obj[key].econ =parseFloat( (obj[key].totalRuns / (obj[key].totalBalls/6)).toFixed(2));
 })

// sort the object using bowler economy

 let sorted = {};
 Object.keys(obj).sort(function (a,b) {
   
   return obj[a].econ - obj[b].econ;
 })
 .slice(0,10) // slice out top ten results
 .map((key)=>{
   sorted[key] = obj[key];
 })
 
 return sorted;

}

// Find the number of times each team won the toss and also won the match

function eachTeamWonTossWithMatch(matchesData) {
  let obj = {};

  matchesData.map((data)=> {
     
    if(obj[data.winner]) {
        
      if(data.toss_winner === data.winner) {
        obj[data.winner].wonTossWithMatch++ ;
      }

    } else {
         
      if(data.toss_winner === data.winner) {
         obj[data.winner] = {};
         obj[data.winner].wonTossWithMatch = 1;
       }
    }
  })
 return obj;
}

// Find a player who has won the highest number of Player of the Match awards for each season
function playerWonHighestPlayerOfMatch(matchesData) {
  let obj = {};

  matchesData.map((data)=> {

    if(obj[data.season] == null) {
      obj[data.season] = {};
      obj[data.season][data.player_of_match] = 1;
    } else {

       if(obj[data.season][data.player_of_match] == null ) {
        obj[data.season][data.player_of_match] = 1;
       } else {
        obj[data.season][data.player_of_match]++;
       }
    }
  })

  let finalObj = {};

Object.keys(obj).map((outerKey)=> {
    let maxCount = 0;
    let playerName = '';

  Object.keys(obj[outerKey]).map((innerKey)=> {

        if(maxCount <= obj[outerKey][innerKey]) {
          maxCount = obj[outerKey][innerKey];
          playerName = innerKey;
        }
  })

  finalObj[outerKey] = {};
  finalObj[outerKey].playerName = playerName;
  finalObj[outerKey].noOfTimePlayerOfMatch = maxCount;


})

return finalObj;
}


// Find the strike rate of a batsman for each season

function strikeRateOfBatsmanForEachSeason(matchesData, deliveriesData) {

let obj = {};
  deliveriesData.map((data)=> {

     if(obj[data.match_id] == null) {

     obj[data.match_id] = {};
     obj[data.match_id][data.batsman] = {};
     obj[data.match_id][data.batsman].runs = parseInt(data.batsman_runs);

     if(data.wide_runs === '0' && data.noball_runs === '0') {

      obj[data.match_id][data.batsman].balls = 1;
     } else {
      obj[data.match_id][data.batsman].balls = 0;
    }
    } else {

      if(obj[data.match_id][data.batsman]) {

        obj[data.match_id][data.batsman].runs += parseInt(data.batsman_runs);

        if(data.wide_runs === '0' && data.noball_runs === '0') {

          obj[data.match_id][data.batsman].balls++;
         } 

      } else {

          obj[data.match_id][data.batsman] = {};

          obj[data.match_id][data.batsman].runs = parseInt(data.batsman_runs);

         if(data.wide_runs === '0' && data.noball_runs === '0') {

           obj[data.match_id][data.batsman].balls = 1;
         } else {
           obj[data.match_id][data.batsman].balls = 0;
         }

       }
    }
     

  })
let copyObj = obj;
  matchesData.map((data)=> {
    if(copyObj[data.id]) {

      copyObj[data.season] = copyObj[data.id];
      delete copyObj[data.id];
      
    }
   
  })


Object.keys(copyObj).map((outerKey)=> {

Object.keys(copyObj[outerKey]).map((innerKey)=> {

  copyObj[outerKey][innerKey].strikeRate = parseFloat(((copyObj[outerKey][innerKey].runs/copyObj[outerKey][innerKey].balls)*100).toFixed(2)); 
      
})
})

   return copyObj;
}

// Find the highest number of times one player has been dismissed by another player

function highNoOfTimePlayerDisByAnotherPlayer(deliveriesData) {
  let obj = {};
  let finalResult = {};
  let maxCount = 1;


  deliveriesData.map((data)=> {

    if(data.dismissal_kind !== 'run out' && data.dismissal_kind !== '' ) {

      if(obj[data.batsman] == null) {
          obj[data.batsman] = {};
          obj[data.batsman][data.bowler] = {};
          obj[data.batsman][data.bowler].noOfTimePlayerDiss = 1;           
      } else {
  //      console.log(data.bowler,obj[data.batsman][data.bowler]);

          if(obj[data.batsman][data.bowler]) {
            obj[data.batsman][data.bowler].noOfTimePlayerDiss++;
          
          if(maxCount <= obj[data.batsman][data.bowler].noOfTimePlayerDiss) {
            maxCount = obj[data.batsman][data.bowler].noOfTimePlayerDiss;
            finalResult.batsman = data.batsman;
            finalResult.bowler  = data.bowler;
            finalResult.noOfTimeDiss = maxCount;
          }
        }
          
        
      }
    }
  })
  return finalResult;
}

//Find the bowler with the best economy in super overs

function bestEcoBowlerInSuperOver( deliveriesData ) {
  let obj = {};
  deliveriesData.map((data)=> {

    if(data.is_super_over === '1') {

      if(obj[data.bowler] == null) {
         obj[data.bowler] = {};

         if(data.wide_runs === '0' && data.noball_runs === '0') {
           obj[data.bowler].totalBalls = 1;
           obj[data.bowler].totalRuns = parseInt(data.total_runs)
         } else {
          obj[data.bowler].totalBalls = 0;
          obj[data.bowler].totalRuns = parseInt(data.total_runs);
         }
        
      } else {

        if(data.wide_runs === '0' && data.noball_runs === '0') {
          obj[data.bowler].totalBalls++;
          obj[data.bowler].totalRuns += parseInt(data.total_runs);
        } else {
          obj[data.bowler].totalRuns += parseInt(data.total_runs);
        }
      }
    }
  })

  Object.keys(obj).map((key)=> {
    obj[key].econ =parseFloat( (obj[key].totalRuns / (obj[key].totalBalls/6)).toFixed(2));
  })

  let sorted = {};
 Object.keys(obj).sort(function (a,b) {
   
   return obj[a].econ - obj[b].econ;
 })
 .slice(0,1) // slice first result
 .map((key)=>{
   sorted[key] = obj[key];
 })

  return sorted;

}

module.exports = { 
  matchesPerYear,
  wonMatchesPerTeamByPerYear,
  extraRunConcededbyPerTeam2016,
  topTenEcoBowler2015,
  eachTeamWonTossWithMatch,
  playerWonHighestPlayerOfMatch,
  highNoOfTimePlayerDisByAnotherPlayer,
  bestEcoBowlerInSuperOver,
  strikeRateOfBatsmanForEachSeason
};

